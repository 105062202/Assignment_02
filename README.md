# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## What I had done

>A complete game process: start menu => game view => game over => quit or play again

以下是我的遊戲流程:<br>
![](flow.png)

>Your game should follow the basic rules of  "小朋友下樓梯".

玩家在畫面中隨重力下落，各種平台隨機生成上飄。
碰觸到尖刺會損血，掉出畫面外即死亡。

>All things in your game should have correct physical properties and behaviors.

所有碰撞均使用phaser的arcade引擎做判斷。

>Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform)

尖刺平台:木刺和鐵刺，鐵刺扣血扣得比木刺多。

彈跳平台:踩到上面會彈起來。

破爛平台:踩到會爆炸，爆炸後即失去平台功能。

一般平台:踩在上面可以回血。


>Add some additional sound effects and UI to enrich your game.

玩家在跳起時會播放隨機用力的聲音(2種)。

玩家在碰到尖刺時會播放隨機受傷的聲音(2種)。

破爛平台在爆炸時會播放爆破聲。

>Store player's name and score in firebase real-time database, and add a leaderboard to your game.

可以在遊戲結束後將自己的成績存進firebase裡。

在主選單可以進入排行榜觀看前五名的紀錄。

>Appearance (subjective)

多張背景圖、平台圖、牆壁、玩家、尖刺、按鈕圖片選自OpenGameArt。

>Other creative features in your game (describe on README.md)

無。

