var mainState = {
    preload: function() {
        // Sprite loading area, shouldn't change anythig here.
        game.load.image('ball', 'assets/ball.png');
        game.load.spritesheet('player', 'assets/hero.png', 16, 16);
    },
    create: function() {

        /******Basic system settings ******/

        game.stage.backgroundColor = '#3498db'; // Set background
        game.renderer.renderSession.roundPixels = true; // Setup renderer

        /// ToDo #1: How can we enable physics in Phaser?
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///

        /******Player******/

        // Add player
        // This can be a hint to finish the following part in this Lab.
        this.player = game.add.sprite(16, 16, 'player');
        this.player.anchor.setTo(0.5, 0.5); // Set anchor to middle of sprite
        this.player.scale.setTo(1.5,1.5);
        game.physics.arcade.enable(this.player);    // Enable Physics
        this.player.enableBody = true;      // Enable Physics
        this.player.body.immovable = false;  // Set player immovable.
        this.player.body.bounce.x = 1;      // Set perfect bounce to bar.
        this.player.body.bounce.y = 1; 
        // Bricks group 
        
        /******Score Label******/ 

        // Display the score
        this.scoreLabel = game.add.text(10, 550, 'score: 0', { font: '25px Arial', fill: '#ffffff' });
        // Initialize the score variable
        this.score = 0;
        

        /******Ball******/ 

        this.ball = null;

        /// ToDo #3: Add ball and its property.
        ///      1. Add ball sprite (named 'ball') to game and assign to this.ball (width, height) = (game.width/2, game.height/2)
        ///      2. Set anchor point of ball to its center.
        ///      3. Enable physics.
        ///      4. Set perfect bounce as player do.

        this.ball=game.add.sprite(game.width/2, game.height/2,'ball');
        this.ball.anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(this.ball);
        this.ball.enableBody=true;
        this.ball.body.bounce.x = 1;
        this.ball.body.bounce.y = 1;



        ///

        // Give ball a initial speed.
        this.ball.body.velocity.y = 300;    //*
        this.ball.body.velocity.x = -75;    //*
        this.ball.body.collideWorldBounds = true;   //*

        // Check bounds collisions against all walls except the bottom one.
        game.physics.arcade.checkCollision.down = false;

        // Enable keyboard event
        this.cursor = game.input.keyboard.createCursorKeys();
    },

    update: function() {
        /// ToDo #4: Things we should check collide:
        ///      1. Bar (player) and ball.
        ///      2. Ball and world bound. (Already checked!)
        ///      3. Ball and bricks.
        /// Note: We should fire a callback function when ball and brick collides.
        ///       It has the same syntax with as 'overlap' just replace as 'collide'
        ///       Hint: game.physics.arcade.overlap(this.player, this.coin, this.takeCoin, null, this);
        game.physics.arcade.collide(this.player, this.ball);
        game.physics.arcade.collide(this.ball, this.bricks,this.hitBlock, null, this);
        ///


        // If the ball goes out of the world, player dies.
        if (!this.ball.inWorld) { this.ballFall();}
        this.moveBar();
    }, 


    ///ToDo #5: Callback function for hitting the block.
    ///     1. Add score.
    ///     2. Update score text.
    ///     3. Kill the brick.
    hitBlock: function(ball, block) {
        this.score++;
        this.scoreLabel.text ='score: '+ this.score;
        block.kill();
    },

    // Restart game while ball falls out the world.
    ballFall: function() { game.state.start('main');},

    // Move bar
    moveBar: function() {


        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -500;}
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 500;}    
        // If neither the right or left arrow key is pressed
        else if (this.cursor.up.isDown) { 
            this.player.body.velocity.y = -500;}   
        else if (this.cursor.down.isDown) { 
            this.player.body.velocity.y = 500;}   
        else {
        // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
        }    
    }
};

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.start('main');



