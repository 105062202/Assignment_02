var bootState = {
    preload: function() {
        game.load.image('progressBar', 'assets/bar.png'); 
    },
    create: function() {
        game.stage.backgroundColor = '#13022D';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        // Start the load state.
        game.state.start('load');
    },
};
var music;
var gamemusic;
var rank_music;
var exp;
var jump;
var jumpplat;
var overgame;
var loadState = {
    preload: function(){
        readranking(0);
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        game.load.spritesheet('player', 'assets/hero.png', 16, 16);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('normal','assets/tile04.png');
        game.load.image('button_start','assets/BTN_BLUE_RECT_OUT.png');
        game.load.image('slow','assets/tile23.png');
        game.load.image('woodspike','assets/SpikeblockWood.png');
        game.load.image('silverspike','assets/SpikeblockSteel.png');
        game.load.image('ceiling','assets/FourspikesWood.png');
        game.load.image('explode','assets/explosion_atlas.png');
        game.load.image('blooding','assets/blood.png');
        game.load.spritesheet('fake','assets/fake.png',64,64);
        /*
        game.load.image('enemy', 'assets/enemy.png');
        game.load.image('coin', 'assets/coin.png');
        game.load.image('wallV', 'assets/wallVertical.png');
        game.load.image('wallH', 'assets/wallHorizontal.png');
        // Load a new asset that we will use in the menu state*/
        game.load.image('background', 'assets/background.jpg');
        game.load.image('bgover','assets/gameover-bg.jpg');
        game.load.image('bgrank','assets/ranking-bg.jpg')
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
        });
        game.load.audio('playmusic', ['assets/through space.ogg']);
        game.load.audio('menumusic', ['assets/nebula.mp3']);
        game.load.audio('rankmusic',['assets/OutThere.ogg']);
        game.load.audio('platformexplode',['assets/8bit_bomb_explosion.wav']);
        game.load.audio('jump_plat',['assets/jump.ogg']);
        game.load.audio('jump1',['assets/jumppp11.ogg']);
        game.load.audio('jump2',['assets/jumppp22.ogg']);
        game.load.audio('hurt1',['assets/ouch0.mp3']);
        game.load.audio('hurt2',['assets/ouch1.mp3']);
        game.load.audio('gameover',['assets/Try again LOOPED.wav']);
        
    },
    create: function(){
        game.state.start('menu'); 
    },
};
var buttonrank;

var menuState = {
    preload: function(){
        game.add.image(0, 0, 'background'); 
        music = game.add.audio('menumusic');
    },
    create: function(){
        readranking(0);
        
        music.play();
        var nameLabel = game.add.text(game.width/2, 80, 'Go Downward',{ font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        var subLabel = game.add.text(game.width/2, 160, 'Try to stay alive',{ font: '30px Arial', fill: '#dddddd' });
        subLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(nameLabel).to({y: 85}, 500).to({y: 75}, 1000).to({y: 80}, 500).loop().start();
        buttonrank= game.add.button(game.world.centerX-60, 450, 'button_start', torank, this);
        buttonrank.scale.setTo(0.3,0.3);
        var rankLabel = game.add.text(game.world.centerX-3, 470,'Ranking', { font: '25px Arial', fill: '#ffffff' });
        rankLabel.anchor.setTo(0.5, 0.5);
        button = game.add.button(game.world.centerX-60, 400, 'button_start', actionOnClick, this);
        button.scale.setTo(0.3,0.3);
        var startLabel = game.add.text(game.world.centerX-3, 420,'Start', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        keyboard.enter.onDown.add(this.start, this); 
        
        document.getElementById('text_container').innerHTML="";
        console.log('cha'+change);
    },
    start: function(){
        music.stop();
        game.global.score=0;
        game.state.start('play');
    }
};
function actionOnClick () {
    music.stop();
    game.global.score=0;
    game.state.start('play');
}
function torank(){
    music.stop();
    game.state.start('rank');
}
var exploding;
var blood_now;
var playState = {
    preload: function(){
        game.add.image(0, 0, 'background'); 
        gamemusic = game.add.audio('playmusic');
    },
    create: function(){
        
        gamemusic.play();
        game.stage.backgroundColor = '#000000';
        //ceiling
        var c1=game.add.sprite(64*0.8,0,'ceiling');
        c1.scale.setTo(0.4,0.4);
        var c2=game.add.sprite(64*1.8,0,'ceiling');
        c2.scale.setTo(0.4,0.4);
        var c3=game.add.sprite(64*2.8,0,'ceiling');
        c3.scale.setTo(0.4,0.4);
        var c4=game.add.sprite(64*3.8,0,'ceiling');
        c4.scale.setTo(0.4,0.4);
        var c5=game.add.sprite(64*4.8,0,'ceiling');
        c5.scale.setTo(0.4,0.4);
        var c6=game.add.sprite(64*5.8,0,'ceiling');
        c6.scale.setTo(0.4,0.4);
        var c7=game.add.sprite(64*6.8,0,'ceiling');
        c7.scale.setTo(0.4,0.4);
        //player
        this.player = game.add.sprite(game.width/2,0,'player');
        this.player.anchor.setTo(0.5, 0.5); // Set anchor to middle of sprite
        this.player.scale.setTo(1.5,1.5);
        game.physics.arcade.enable(this.player);    // Enable Physics
        this.player.enableBody = true;      // Enable Physics
        this.player.body.immovable = false;
        this. player.frame = 12;
        this.player.body.gravity.y=500;
        this.player.position.x=game.width/2;
        this.player.position.y=40;
        this.player.animations.add('left', [4, 5, 6, 7], 8);
        this.player.animations.add('right', [4, 5, 6, 7], 8);
        this.player.animations.add('jumpleft', [8, 9, 10, 11], 8);
        this.player.animations.add('jumpight', [8, 9, 10, 11], 8);
        this.player.animations.add('fall', [12, 13, 14, 15], 8);
        this.player.life=50;
        this.player.unbeatableTime = 0;
        //wall
        leftwall = game.add.sprite(0,0,'wall');
        leftwall.scale.setTo(0.8,2.34375);
        game.physics.arcade.enable(leftwall);
        leftwall.body.immovable = true;
        rightwall = game.add.sprite(500-64*0.8,0,'wall');
        rightwall.scale.setTo(0.8,2.34375);
        game.physics.arcade.enable(rightwall);
        rightwall.body.immovable = true;
        //text
        var style = {fill: '#ff0000', fontSize: '20px'}
        lifetext = game.add.text(64*0.8, 10, '', style);
        scoretext = game.add.text(64*0.8, 30, '', style);
        //effect
        
    },
    update: function(){
        this.physics.arcade.collide(this.player, [leftwall, rightwall]);
        this.physics.arcade.collide(this.player,platforms,effect);
        playerupdate (this.player);
        gameover(this.player);
        // platform
        platformcreate();
        platformupdate();
        //text
        textupdate(this.player);
        //ceiling
        ceiling(this.player)
    }
};
var overState = {
    preload:function(){
        game.add.image(0, 0, 'bgover'); 
    },
    create: function(){
        overgame = game.add.audio('gameover');
        overgame.play();
        var OVE = game.add.text(game.width/2, 80, 'GAME OVER',{ font: '25px Arial', fill: '#ff0000' });
        OVE.anchor.setTo(0.5, 0.5);
        var OVER = game.add.text(game.width/2, 110, 'Please submit your score',{ font: '18px Arial', fill: '#ff0000' });
        OVER.anchor.setTo(0.5, 0.5);
        var OVERscore = game.add.text(game.width/2, 130, game.global.score+" points",{ font: '18px Arial', fill: '#ff0000' });
        OVERscore.anchor.setTo(0.5, 0.5);
        button_re = game.add.button(game.world.centerX-60, 450, 'button_start', this.start, this);
        button_re.scale.setTo(0.3,0.3);
        document.getElementById('text_container').innerHTML="<input id='name' maxlength='10' placeholder='player name'>";
        var submitLabel = game.add.text(game.world.centerX-3, 470,'Submit', { font: '25px Arial', fill: '#ffffff' });
        submitLabel.anchor.setTo(0.5, 0.5);
    },
    update: function(){
    },
    start: function(){/*
        firebase.database().ref('ranking').update({//change here
            a: 5,
            b: 4,
            c: 3,
            d: 2,
            e: 1
        }).catch(e => {
            console.log(e);
        });*/
        //get first
        var playername=document.getElementById('name').value;
        var temp_n;
        var temp_s;
        if(!playername)playername="anonymous";
        if(game.global.score>scoreranking[0]){
            console.log('1');
            rankRefa.update({
                name: playername,
                score: game.global.score
            });
            rankRefb.update({
                name: nameranking[0],
                score: scoreranking[0]
            });
            
            rankRefc.update({
                name: nameranking[1],
                score: scoreranking[1]
            });
            rankRefd.update({
                name: nameranking[2],
                score: scoreranking[2]
            });
            rankRefe.update({
                name: nameranking[3],
                score: scoreranking[3]
            });
        }
        else if(game.global.score>scoreranking[1]){
            console.log('2');
            rankRefb.update({
                name: playername,
                score: game.global.score
            });
            rankRefc.update({
                name: nameranking[1],
                score: scoreranking[1]
            });
            rankRefd.update({
                name: nameranking[2],
                score: scoreranking[2]
            });
            rankRefe.update({
                name: nameranking[3],
                score: scoreranking[3]
            });
        }
        else if(game.global.score>scoreranking[2]){
            console.log('3');
            rankRefc.update({
                name: playername,
                score: game.global.score
            });
            rankRefd.update({
                name: nameranking[2],
                score: scoreranking[2]
            });
            rankRefe.update({
                name: nameranking[3],
                score: scoreranking[3]
            });
        }
        else if(game.global.score>scoreranking[3]){
            console.log('4');
            rankRefd.update({
                name: playername,
                score: game.global.score
            });
            rankRefe.update({
                name: nameranking[3],
                score: scoreranking[3]
            });
        }
        else if(game.global.score>scoreranking[4]){
            console.log('5');
            rankRefe.update({
                name: playername,
                score: game.global.score
            });
        }
        overgame.stop();
        game.state.start('menu');
        
    }
};
var rankState={
    preload:function(){
        game.add.image(0, 0, 'bgrank'); 
    },
    create:function(){
        rank_music = game.add.audio('rankmusic');
        rank_music.play();
        readranking(1);
        var Label = game.add.text(game.world.centerX, 100,'Top 5', { font: '25px Arial', fill: '#ffff55' });
        Label.anchor.setTo(0.5,0.5);
        button_re = game.add.button(game.world.centerX-60, 450, 'button_start', this.start, this);
        button_re.scale.setTo(0.3,0.3);
        var submitLabel = game.add.text(game.world.centerX-3, 470,'Menu', { font: '25px Arial', fill: '#ffffff' });
        submitLabel.anchor.setTo(0.5, 0.5);
    },
    start:function(){
        rank_music.stop();
        game.state.start('menu');
    }
};
function readranking (flag){
    var scoreLabel
    rankRefa.once('value')
        .then(function (snapshot) {
                snapshot.forEach(function(childSnapshot){
                    if(childSnapshot.key=="name"){
                        nameranking[0]=childSnapshot.val();
                        if(flag){
                            scoreLabel=game.add.text(game.width/2, game.height/4+(30*1),
                            nameranking[0]+':', { font: '25px Arial', fill: '#ffffff' });
                            scoreLabel.anchor.setTo(0.5, 0.5);
                        }
                        
                    }
                    else{
                        scoreranking[0]=childSnapshot.val();
                        if(flag){
                            scoreLabel.setText(scoreLabel.text+scoreranking[0]);
                        }
                    }
                   
                })
        })
        .catch(e => console.log(e.message));
    //get second
    rankRefb.once('value')
    .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                if(childSnapshot.key=="name"){
                    nameranking[1]=childSnapshot.val();
                    if(flag){
                        scoreLabel=game.add.text(game.width/2, game.height/4+(30*2),
                        nameranking[1]+':', { font: '25px Arial', fill: '#ffffff' });
                        scoreLabel.anchor.setTo(0.5, 0.5);
                    }
                    
                }
                else{
                    scoreranking[1]=childSnapshot.val();
                    if(flag){
                        scoreLabel.setText(scoreLabel.text+scoreranking[1]);
                    }
                    
                }
            
            })
    })
    .catch(e => console.log(e.message));
    //get third
    rankRefc.once('value')
        .then(function (snapshot) {
                snapshot.forEach(function(childSnapshot){
                    if(childSnapshot.key=="name"){
                        nameranking[2]=childSnapshot.val();
                        if(flag){
                        scoreLabel=game.add.text(game.width/2, game.height/4+(30*3),
                        nameranking[2]+':', { font: '25px Arial', fill: '#ffffff' });
                            scoreLabel.anchor.setTo(0.5, 0.5);
                        }
                        
                    }
                    else{
                        scoreranking[2]=childSnapshot.val();
                        if(flag){
                        scoreLabel.setText(scoreLabel.text+scoreranking[2]);
                        }
                        
                    }
                   
                })
        })
        .catch(e => console.log(e.message));
    //get fourth
    rankRefd.once('value')
        .then(function (snapshot) {
                snapshot.forEach(function(childSnapshot){
                    if(childSnapshot.key=="name"){
                        nameranking[3]=childSnapshot.val();
                        if(flag){
                        scoreLabel=game.add.text(game.width/2, game.height/4+(30*4),
                        nameranking[3]+':', { font: '25px Arial', fill: '#ffffff' });
                            scoreLabel.anchor.setTo(0.5, 0.5);
                        }
                        
                    }
                    else{
                        scoreranking[3]=childSnapshot.val();
                        if(flag){
                        scoreLabel.setText(scoreLabel.text+scoreranking[3]);
                        }
                        
                    }
                   
                })
        })
        .catch(e => console.log(e.message));
    //get fifth
    rankRefe.once('value')
        .then(function (snapshot) {
                snapshot.forEach(function(childSnapshot){
                    if(childSnapshot.key=="name"){
                        nameranking[4]=childSnapshot.val();
                        if(flag){
                        scoreLabel=game.add.text(game.width/2, game.height/4+(30*5),
                        nameranking[4]+':', { font: '25px Arial', fill: '#ffffff' });
                            scoreLabel.anchor.setTo(0.5, 0.5);
                        }
                        
                    }
                    else{
                        scoreranking[4]=childSnapshot.val();
                        if(flag){
                        scoreLabel.setText(scoreLabel.text+scoreranking[4]);
                        }
                        
                    }
                   
                })
        })
        .catch(e => console.log(e.message));
}
function playerupdate (player) {
    if(keyboard.up.isDown) {
        if(player.body.touching.down){
            player.body.velocity.y = -350;
            var rand = Math.random();
            if(rand<0.5){
                jump=game.add.audio('jump1');
                jump.play();
            }
            else{
                jump=game.add.audio('jump2');
                jump.play();
            }
        }
    }else if(keyboard.left.isDown) {
        player.scale.setTo(-1.5,1.5);
        player.body.velocity.x = -250;
    }else if(keyboard.right.isDown) {
        player.scale.setTo(1.5,1.5);
        player.body.velocity.x = 250;
    }
     else {
        player.body.velocity.x = 0;
    }
    playeranimate(player);
}
function playeranimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;
    if (x < 0 && y > 0) {
        player.animations.play('jumpleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('jumpleft');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fall');
    }
    if (x == 0 && y == 0) {
      player.frame = 12;
    }
}
function platformcreate(){
    if(game.time.now>last+600){
        last=game.time.now;
        game.global.score+=1;
        var platform;
        var x = Math.random()*(400- 64) + 64*0.8;
        var y = 600;
        var rand = Math.random() * 100;
        if(rand < 30) {
            platform = game.add.sprite(x, y, 'normal');
            platform.scale.setTo(1,0.25);
        }else if(rand<50){
            platform = game.add.sprite(x, y, 'slow');
            platform.scale.setTo(1,0.25);
        } else if(rand<70){
            platform = game.add.sprite(x, y, 'woodspike');
            platform.scale.setTo(64/152,16/84);
        }else if(rand<75){
            
            platform = game.add.sprite(x, y, 'silverspike');
            platform.scale.setTo(64/145,16/84);
        }else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1], 14);
            last+=600;
        }
        game.physics.arcade.enable(platform);
        platform.enableBody=true;
        platform.body.immovable=true;
        platforms.push(platform);
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
        //platform = game.add.sprite(x, y, 'normal');
        

        
    }
}
function platformupdate(){
    for(var i=0; i<platforms.length; i++) {
        var plat = platforms[i];
        if(plat.body){
            plat.body.position.y -= 2;
            if(plat.body.position.y <= -20) {
                plat.destroy();
                platforms.splice(i, 1);
            }
        }
        
    }
}
function effect(player,platform){
    if(platform.key=='fake'){
        if(player.touchOn !== platform) {
            platform.animations.play('turn');
            exp=game.add.audio('platformexplode');
            exp.play();
            exploding=game.add.emitter(platform.position.x+32, platform.position.y, 15);
            exploding.makeParticles('explode');
            exploding.setYSpeed(-150, 150);
            exploding.setXSpeed(-150, 150);
            exploding.setScale(2, 0, 2, 0, 800);
            exploding.gravity = 0;
            exploding.start(true,800, null, 15);
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player.touchOn = platform;
        }
    }else if(platform.key=='woodspike'){
        if (player.touchOn !== platform) {
            player.life -= 7;
            player.touchOn = platform;
            game.camera.flash(0xff9900, 100);
            hurt();
            blood_now=game.add.emitter(player.position.x, player.position.y, 3);
            blood_now.makeParticles('blooding');
            blood_now.setYSpeed(-150, 150);
            blood_now.setXSpeed(-150, 150);
            blood_now.setScale(2, 0, 2, 0, 800);
            blood_now.gravity = 0;
            blood_now.start(true,800, null, 15);
        }
    }else if(platform.key=='silverspike'){
        if (player.touchOn !== platform) {
            player.life -= 15;
            player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
            hurt();
            blood_now=game.add.emitter(player.position.x, player.position.y, 7);
            blood_now.makeParticles('blooding');
            blood_now.setYSpeed(-150, 150);
            blood_now.setXSpeed(-150, 150);
            blood_now.setScale(2, 0, 2, 0, 800);
            blood_now.gravity = 0;
            blood_now.start(true,800, null, 15);
        }
    }else if(platform.key=='normal'){
        if (player.touchOn !== platform) {
            if(player.life < 50) {
                player.life += 3;
            }
            player.touchOn = platform;
        }
    }else if(platform.key=='slow'){
        player.body.velocity.y=-350;
        jumpplat = game.add.audio('jump_plat');
        jumpplat.play();
    }

}
function ceiling(player){
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 7;
            game.camera.flash(0xff9900, 100);
            hurt();
            blood_now=game.add.emitter(player.position.x, player.position.y, 3);
            blood_now.makeParticles('blooding');
            blood_now.setYSpeed(-150, 150);
            blood_now.setXSpeed(-150, 150);
            blood_now.setScale(2, 0, 2, 0, 800);
            blood_now.gravity = 0;
            blood_now.start(true,800, null, 15);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}
function hurt(){
    var rand = Math.random();
    var hurting;
    if(rand<0.5){
        hurting = game.add.audio('hurt1');
        hurting.play();
    }else{
        hurting = game.add.audio('hurt2');
        hurting.play();
    }
}
function textupdate(player){
    lifetext.setText('life:' + player.life);
    scoretext.setText('score:' + game.global.score);
}
function gameover (player) {
    if(player.life <= 0 ||player.body.y > 600) {
        gamemusic.stop();
        game.state.start('over'); 
    }
}
var game = new Phaser.Game(500, 600, Phaser.AUTO, 'canvas');
var lifetext;
var scoretext;
var change = "";
var button;
var button_re;
var last=-600;
var keyboard;
var leftwall;
var rightwall;
var platforms = [];
game.global = { score: 0 }; 
var rankRefa = firebase.database().ref('ranking/a');
var rankRefb = firebase.database().ref('ranking/b');
var rankRefc = firebase.database().ref('ranking/c');
var rankRefd = firebase.database().ref('ranking/d');
var rankRefe = firebase.database().ref('ranking/e');
var scoreranking = [5];
var nameranking = [5];
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('rank', rankState)
game.state.add('over', overState);
game.state.start('boot');

